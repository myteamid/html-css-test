import React, { Component } from 'react'
import Slider               from 'react-slick'

/*style*/
import './style.scss'

/*data*/
import {iconBoxData} from '../../data'

import hero1                from '../../assets/images/hero/stock-photo-168976409.jpg'
import hero2                from '../../assets/images/hero/image_back1.jpg'

/*custom*/
import IconBox              from '../iconBox'


class Hero extends Component {


    render() {

        const settings = {
            dots: false,
            infinite: true,
            autoplay: true,
            fade: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
        };

        const heroStyles1 = { backgroundImage: `url(${hero1})` };

        const heroStyles2 = { backgroundImage: `url(${hero2})` };

        return (
            <section className="hero">
                <div className="hero_list">
                    <Slider {...settings}>
                        <div className="hero_item">
                            <div className="hero_bg" style={heroStyles1}></div>
                        </div>
                        <div className="hero_item">
                            <div className="hero_bg" style={heroStyles2}></div>
                        </div>
                    </Slider>
                </div>
                <div className="hero_content">
                    <div className="hero_inner">
                        <div className="hero_title">
                            <h2 className="hero_title-big">Stu Unger Rise And Fall Of A Poker Genius</h2>
                            <div className="hero_title-small">
                                <p> V7 Digital Photo Printing</p>
                            </div>
                        </div>
                        <div className="hero_icon">
                            {iconBoxData.map(item => {
                                return <IconBox key={item.id} icon={item.icon} title={item.title}/>
                            })}
                        </div>
                    </div>

                </div>
            </section>
        );
    }

};

export default Hero;

