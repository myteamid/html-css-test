import React     from 'react'
import PropTypes from "prop-types"

/*style*/
import './style.scss'


const IconBox = ({ icon, title }) => {
    return (
        <div className="icon-box">
            <div className="icon-box_img">
                {icon}
            </div>
            <div className="icon-box_title">
                {title}
            </div>
        </div>
    );
};

IconBox.propTypes = {
    icon: PropTypes.any,
    title: PropTypes.string
};

export default IconBox;

