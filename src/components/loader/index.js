import React from 'react'

/*style*/
import './style.scss'

const Loader = () => (
    <div className="load">
        <div className="load_item"> </div>
    </div>
);

export default Loader;