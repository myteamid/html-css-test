import React, { Component } from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import Loader               from '../loader'
import Button               from '../button'
import NewsList 			from './newsGrid'

class NewsGridContainer extends Component {
	state = {
		error: null,
		isLoaded: false,
		endPosts: false,
		itemsLoad: [],
		itemsShown: []
	};


	componentDidMount() {
		fetch("https://jsonplaceholder.typicode.com/posts")
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({
						isLoaded: true,
						itemsLoad: result,
						itemsShown: result.slice(0, 6)
					});
				},
				(error) => {
					this.setState({
						isLoaded: true,
						error
					});
				}
			)
	}


	moreLoad = () => {
		let { itemsShown, itemsLoad } = this.state;

		itemsShown = itemsLoad.slice(0, itemsShown.length + 6);

		if (itemsShown.length === itemsLoad.length) {
			this.setState({ endPosts: true, itemsShown })
		}
		this.setState({ itemsShown })
	};


	render() {
		const { error, endPosts, isLoaded, itemsShown } = this.state;

		const renderContent = () => {
			if ( error ) {
				return <div>Error: {error.message}</div>;
			} else if ( isLoaded ) {
				return <NewsList itemsShown={itemsShown}/>
			}
			return <Loader/>
		};

		return (
			<div>
				<div className="container">
					<ReactCSSTransitionGroup
					transitionName="opacity-animate"
					transitionEnterTimeout={500}
					transitionLeaveTimeout={300}>
					{renderContent()}
					</ReactCSSTransitionGroup>

				</div>

				<div className="section_btn">
					<hr/>
					{/*<ReactCSSTransitionGroup*/}
					{/*transitionName="news-grid"*/}
					{/*transitionEnterTimeout={500}*/}
					{/*transitionLeaveTimeout={300}>*/}
					{!endPosts ?
						<Button text="More news" className="btn--orange" onClick={this.moreLoad}/>
						:
						null}
					{/*</ReactCSSTransitionGroup>*/}

				</div>
			</div>
		);
	}
}

export default NewsGridContainer;
