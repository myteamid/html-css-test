import React     from 'react'

/*style*/
import './style.scss'
import PropTypes from "prop-types";


const Sandwich = ({ openMenu, handleClick }) => {


    return (
        <div className={`sandwich ${openMenu ? 'is-active' : ''}`}>
            <button className='sandwich_inner' onClick={handleClick}>
                <span className="sandwich_top"> </span>
                <span className="sandwich_middle"> </span>
                <span className="sandwich_bottom"> </span>
            </button>
        </div>

    )
};

Sandwich.propTypes = {
    openMenu: PropTypes.bool,
    handleClick: PropTypes.func,
};

export default Sandwich;