import React           from 'react'

/*components*/
import TitleSection    from '../titleSection'
import VacanciesItem   from '../vacanciesItem'

/*style*/
import './style.scss'

/*data*/
import {dataVacancies} from '../../data'


const Vacancies = () => {

    const renderItems = dataVacancies.map(item => (
        <div key={item.id} className="vacancies-grid_col">
            <VacanciesItem icon={item.icon}
                           smallTitle={item.smallTitle}
                           bigTitle={item.bigTitle}
                           mediumTitle={item.mediumTitle}/>
        </div>
    ));

    return (
        <section className="section">
            <div className="container">
                <TitleSection title="Hot vacancies"/>

                <div className="vacancies-grid">
                    {renderItems}
                </div>
            </div>
            <div className="section_btn">
                <hr/>
                <a href="#0" className="btn btn--orange">More vacancies</a>
            </div>
        </section>
    )
};

export default Vacancies;