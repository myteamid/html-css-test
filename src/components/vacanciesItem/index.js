import React     from 'react'
import PropTypes from "prop-types"

/*styles*/
import './style.scss'


const VacanciesItem = (props) => {

    const { icon, smallTitle, bigTitle, mediumTitle } = props;
    return (
        <a href={"#0"} className="vacancies-item">
            <div className="vacancies-item_icon">
                {icon}
            </div>
            <div className="vacancies-item_body">
                <p className="vacancies-item_small-title">{smallTitle}</p>
                <p className="vacancies-item_big-title">{bigTitle}</p>
                <p className="vacancies-item_medium-title">{mediumTitle}</p>
            </div>
        </a>
    )
};

VacanciesItem.propTypes = {
    icon: PropTypes.any,
    smallTitle: PropTypes.string,
    bigTitle: PropTypes.string,
    mediumTitle: PropTypes.string,
};

export default VacanciesItem;