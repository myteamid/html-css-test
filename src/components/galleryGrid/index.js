import React                from 'react'
import LazyLoad             from 'react-lazy-load';

/*components*/
import TitleSection         from '../titleSection'
import GalleryGridContainer from './galleryGridContainer'

/*style*/
import './style.scss'


const GalleryGrid = () => {
	return (
		<section className="section">
			<div className="container">
				<TitleSection title="Inside company"/>
			</div>
			<LazyLoad offsetVertical={400}>
				<GalleryGridContainer/>
			</LazyLoad>

		</section>
	)
};

export default GalleryGrid;
