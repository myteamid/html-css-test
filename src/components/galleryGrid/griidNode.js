import React, { Component, Fragment } from 'react'
import PropTypes                      from 'prop-types';

/*components*/
import Gallery from '../gallery'


export class GridNode extends Component {
    static get propTypes() {
        return {
            items: PropTypes.array,
            type: PropTypes.string,
			dataPopup: PropTypes.func
        };
    }

    _renderSingleItem(item) {
        return (
            <div className="gallery-grid_row">
                <div className="gallery-grid_col">
                    <Gallery itemData={item} onClick={this.props.dataPopup} />
                </div>
            </div>
        );
    }

    _renderDoubleItem(item_1, item_2) {
        return (
            <div className="gallery-grid_row">
                <div className="gallery-grid_col-6">
                    <Gallery itemData={item_1} onClick={this.props.dataPopup}/>
                </div>
                <div className="gallery-grid_col-6">
                    <Gallery itemData={item_2} onClick={this.props.dataPopup}/>
                </div>
            </div>
        );
    }

    _renderGridItems() {
        const {
            type: gridType,
            items
        } = this.props;
        if (gridType === 'top') {
            return (
                <Fragment>
                    {this._renderDoubleItem(items[ 0 ], items[ 1 ])}
                    {this._renderSingleItem(items[ 2 ])}
                </Fragment>
            );
        }

        return (
            <Fragment>
                {this._renderSingleItem(items[ 0 ])}
                {this._renderDoubleItem(items[ 1 ], items[ 2 ])}
            </Fragment>
        );
    }


    render() {
        return (
            <div className="gallery-grid_node">
                {this._renderGridItems()}
            </div>
        );
    }
}

export default GridNode;