import React, { Component } from 'react'

/*components*/
import Hero        from './components/hero'
import Header      from './components/header'
import News        from './components/news'
import GalleryGrid from './components/galleryGrid'
import Vacancies   from './components/vacancies'
import Footer      from './components/footer'


class App extends Component {
    render() {
        return (
            <div className="App">
                <Header/>
                <Hero/>
                <News/>
                <GalleryGrid/>
                <Vacancies/>
                <Footer/>
            </div>
        );
    }
}

export default App;
