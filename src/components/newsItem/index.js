import React     from 'react'
import PropTypes from "prop-types"

/*style*/
import './style.scss'

const NewsItem = (props) => {

    const {img, title, content } = props;

    return (
        <a href={'#0'} className="news-item">
            <div className="news-item_img">
                <img src={img} alt="news bg"/>
            </div>
            <div className="news-item_body">
                <h4 className="news-item_title">{title}</h4>
                <div className="news-item_content">
                    <p>{content}</p>
                </div>
                <div className="news-item_link">Read me</div>
            </div>

        </a>
    )
};

NewsItem.propTypes = {
    img: PropTypes.string,
    title: PropTypes.string,
    content: PropTypes.string
};

export default NewsItem;