import React     from 'react'
import PropTypes from "prop-types"


/*style*/
import './style.scss'

const TitleSection = ({title}) =>  <h3 className="title-section">{title}</h3>;

TitleSection.propTypes = {
    title: PropTypes.string,
};

export default TitleSection;