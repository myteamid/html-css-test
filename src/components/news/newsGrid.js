import React                   from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import PropTypes               from "prop-types"


/*components*/
import NewsItem                from "../newsItem"

/*styles*/
import './newsGrid.scss'


const NewsGrid = ({ itemsShown }) => {
    const renderItems = itemsShown.map(item => (
        <div key={item.id} className="news-grid_col">
            <NewsItem title={item.title} content={item.body} img="https://picsum.photos/470/290/"/>
        </div>
    ));

    return (
        <div>
            <ReactCSSTransitionGroup
                component="div"
                className="news-grid"
                transitionName="opacity-animate"
                transitionEnterTimeout={500}
                transitionLeaveTimeout={300}>
                {renderItems}
            </ReactCSSTransitionGroup>
        </div>

    )
};

NewsGrid.propTypes = {
    itemsShown: PropTypes.array
};

export default NewsGrid;