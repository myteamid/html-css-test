import React from 'react'

/*components*/
import Form from '../form'

/*style*/
import './style.scss'

const Newsletter = () => {

    return (
        <div className="newsletter">
            <div className="newsletter_head">
                <h3 className="newsletter_title">Sign up for newsleter!</h3>
                <p className="newsletter_subtitle">Stay informed of the last company news</p>
            </div>
            <div className="newsletter_form">
                <Form/>
            </div>
        </div>
    )
};

export default Newsletter;