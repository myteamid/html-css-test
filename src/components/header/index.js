import React, { Component } from 'react'

/*components*/
import Nav                  from '../nav'
import Button               from '../button'
import Sandwich             from '../sandwich'

/*styles*/
import './style.scss'

/*images*/
import LogoSvg              from '../../assets/images/icon/layers.svg'


class Header extends Component {
    state = {
        open: false
    };

    toggleMenu = () => {
        this.setState({ open: !this.state.open })
    };


    render() {
        const { open } = this.state;
        return (
            <section className="header">
                <div className="container">
                    <div className="header_inner">
                        <div className="header_left">
							<a href="#0" className="header_logo">
								<LogoSvg wight={50} height={50}/>
                            </a>
                        </div>
                        <div className="header_nav">
                            <Nav openMenu={open}/>
                            <Sandwich handleClick={this.toggleMenu} openMenu={open}/>
                        </div>
                        <div className="header_right">
                            <Button className="btn--transparent" text="Log in"/>
                            <Button className="btn--blue" text="Sing in"/>
                        </div>
                    </div>

                </div>
            </section>
        );
    }

};

export default Header;

