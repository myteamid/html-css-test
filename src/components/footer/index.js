import React      from 'react'

/*components*/
import Newsletter from '../newsletter'

/*img*/
import FooterBg from '../../assets/images/footer.jpg'

/*style*/
import './style.scss'

const Footer = () => {

    return (
        <footer className="footer" style={{backgroundImage: `url(${FooterBg})`}}>
            <div className="container">
                <Newsletter/>
            </div>
        </footer>
    )
};

export default Footer;
