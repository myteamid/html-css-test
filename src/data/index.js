import React from 'react'

/*img*/
import Star                 from '../assets/images/icon/star.svg'
import Clock                from '../assets/images/icon/clock.svg'
import MusicPlayer          from '../assets/images/icon/music-player.svg'
import House                from '../assets/images/icon/house.svg'
import IconSettings    from '../assets/images/icon/settings.svg'
import IconPlaceholder from '../assets/images/icon/placeholder.svg'
import IconPhone       from '../assets/images/icon/phone-call.svg'
import IconEnvelope    from '../assets/images/icon/envelope.svg'
import IconAvatar      from '../assets/images/icon/avatar.svg'
import IconLike        from '../assets/images/icon/like.svg'

export const iconBoxData = [
    {
        'icon': <Clock wight={100} height={100}/>,
        'title': 'The Skinny On Lcd Monitors',
        'id': '12dasd'
    },
    {
        'icon': <Star wight={100} height={100}/>,
        'title': 'Guidelines For Inkjet Cartridge Refill',
        'id': 'asda'
    },
    {
        'icon': <MusicPlayer wight={100} height={100}/>,
        'title': 'Do A Sporting Stag Do In Birmingham',
        'id': 'htfhd'
    },
    {
        'icon': <House wight={100} height={100}/>,
        'title': 'Your Computer Usage',
        'id': '34tge'
    }
];



export const dataVacancies = [
    {
        "icon": <IconSettings wight={60} height={60}/>,
        "smallTitle": "A situation",
        "bigTitle": "As soon as Computerized Tomography",
        "mediumTitle": "No image is so essential",
        "id": "1"
    },
    {
        "icon": <IconEnvelope wight={60} height={60}/>,
        "smallTitle": "A situation",
        "bigTitle": "Magnetic Resonance Imaging",
        "mediumTitle": "No image is so essential",
        "id": "2"
    },
    {
        "icon": <IconAvatar wight={60} height={60}/>,
        "smallTitle": "A situation",
        "bigTitle": "This is for the reason",
        "mediumTitle": "No image is so essential",
        "id": "3"
    },
    {
        "icon": <IconLike wight={60} height={60}/>,
        "smallTitle": "A situation",
        "bigTitle": "After that, in the 1980s Magnetic",
        "mediumTitle": "No image is so essential",
        "id": "4"
    },
    {
        "icon": <IconPhone wight={60} height={60}/>,
        "smallTitle": "A situation",
        "bigTitle": "became accessible in the 1970s",
        "mediumTitle": "No image is so essential",
        "id": "5"
    },
    {
        "icon": <IconPlaceholder wight={60} height={60}/>,
        "smallTitle": "A situation",
        "bigTitle": "MRIs concentrate on water molecules",
        "mediumTitle": "No image is so essential",
        "id": "6"
    },
]