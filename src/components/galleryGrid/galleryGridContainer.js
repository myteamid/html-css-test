import React, { Component, Fragment } from 'react'
import _                    from 'lodash'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

/*components*/
import GridNode             from './griidNode'
import Loader               from '../loader'
import Popup from '../popup'


class GalleryGridContainer extends Component {
	state = {
		openPopup: false,
		popupUrl: '',
		error: null,
		isLoaded: false,
		endPosts: false,
		items: []
	};

	handlePopup = (e) => {
		e.preventDefault();
		this.setState({
			openPopup: !this.state.openPopup,
			popupUrl: e.target.href
		})
	}


	componentDidMount() {
		fetch("https://jsonplaceholder.typicode.com/photos")
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({
						isLoaded: true,
						items: result
					});
				},
				(error) => {
					this.setState({
						isLoaded: true,
						error
					});
				}
			)
	}

	render() {
		const { items, isLoaded, openPopup } = this.state;
		items.length = 12;
		const itemsWithChunks = _.chunk(items, 3);

		const renderedGrid = itemsWithChunks.map((chunk, i) => {
			if ( i % 2 ) {
				return (
					<GridNode
						key={i}
						items={chunk}
						type="top"
						dataPopup={this.handlePopup}
					/>
				);
			}

			return (
				<GridNode
					key={i}
					items={chunk}
					dataPopup={this.handlePopup}
				/>
			);
		});

		const galleryGrid = () => (
			<Fragment>
				<div className="gallery-grid">
					{renderedGrid}
				</div>
				<ReactCSSTransitionGroup
					transitionName="opacity-animate"
					transitionEnterTimeout={500}
					transitionLeaveTimeout={300}>
					{openPopup ? <Popup url={this.state.popupUrl} closePopup={this.handlePopup}/> : null }
				</ReactCSSTransitionGroup>
			</Fragment>
		)
		return (
			<div>
				{!isLoaded ? <Loader/> : galleryGrid() }
			</div>
		)
	}
}

export default GalleryGridContainer;