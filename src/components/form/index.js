import React, {Component} from 'react'

/*styles*/
import './style.scss'


class Form extends Component {
    constructor(props) {
        super(props);

        this.state = {
            newsletterEmail: '',
            text_message: '',
            formErrors: { email: '' },
            newsletterEmailValid: false,
            formValid: false
        };

        this.validClass = 'is-valid';
        this.inValidClass = 'is-invalid';
    };

    validateField = (fieldName, value) => {
        let { formErrors, newsletterEmailValid } = this.state;

        switch ( fieldName ) {
            case 'newsletterEmail' :
                newsletterEmailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                formErrors.email = newsletterEmailValid ? this.validClass : this.inValidClass;
                break;
            default:
                break;
        }

        this.setState({
            formErrors,
            newsletterEmailValid,
        }, this.validateForm);
    };

    validateForm = () => {
        this.setState({
            formValid: this.state.newsletterEmailValid
        });
    };

    handleChange = (e) => {
        const { value, name } = e.target;
        this.setState({ [ name ]: value },
            () => { this.validateField(name, value) });
    };

    render() {
        const {formErrors, formValid} = this.state;
        return (
            <form action="#" className="form">
                <div className="form_block">
                    <label htmlFor="newsletterEmail" className={formErrors.email}>
                        <input type="mail" name="newsletterEmail"
                               placeholder="Your email"
                               onChange={this.handleChange}/>
                    </label>
                </div>
                <div className="form_btn">
                    <button disabled={!formValid} className="btn btn--submit">Subscribe</button>
                </div>
            </form>
        )
    }
}

export default Form;
