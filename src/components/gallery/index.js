import React     from 'react'
import PropTypes from 'prop-types'
import _         from 'lodash'

/*style*/
import './style.scss'


const Gallery = ({ itemData, onClick }) => {
    if (_.has(itemData, 'url')) {
        return (
            <a href={itemData.url}
               className="gallery"
               onClick={onClick}
               style={{ backgroundImage: `url(${itemData.url})` }}> </a>
        )
    } else {
        return null
    }

};

Gallery.propTypes = {
    itemData: PropTypes.object,
	dataPopup: PropTypes.func
};

export default Gallery;