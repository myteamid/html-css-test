import React, { Component } from 'react'

/*img*/
import IconClose from '../../assets/images/icon/close.svg'

/*style*/
import './style.scss'

class Popup extends Component {


	render() {
		const {url, closePopup} = this.props;
		console.log(url);
		return (
			<div className="popup">
				<button className="popup_close" onClick={closePopup}><IconClose wight={30} height={30}/></button>
				<div className="popup_inner">
					<div className="popup_content">
						<img src={url} alt="popup"/>
					</div>
				</div>
			</div>
		)
	}
}

export default Popup;