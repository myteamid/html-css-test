import React, { Component }    from 'react'
import LazyLoad                from 'react-lazy-load'


/*components*/
import TitleSection            from '../titleSection'
import NewsGridContainer       from './newsGrigContainer'


/*style*/
import './style.scss'


class News extends Component {


	render() {


		return (
			<section className="section news">
				<div className="container">
					<TitleSection title="Latest News"/>
				</div>

					<LazyLoad offsetVertical={400} >
						<NewsGridContainer/>
					</LazyLoad>

			</section>
		)
	}

};

export default News;