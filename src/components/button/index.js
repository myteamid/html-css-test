import React from 'react'

/*style*/
import './style.scss'

const Button = ({className, text, onClick}) => <button className={`btn ${className}`} onClick={onClick}>{text}</button>;

export default Button;

