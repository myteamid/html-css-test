import React     from 'react'
import PropTypes from "prop-types"

/*styles*/
import './style.scss'


const Nav = ({openMenu}) => {
	const activeClass = openMenu ? 'is-active' : '';

    return (
				<nav className={`nav ${activeClass}`}>
					<ul className='nav_list'>
						<li className='nav_item'>
							<a href="#0">About us</a>
						</li>
						<li className='nav_item'>
							<a href="#0">Company</a>
						</li>
						<li className='nav_item'>
							<a href="#0">Career</a>
						</li>
						<li className='nav_item'>
							<a href="#0">Contact</a>
						</li>
					</ul>
				</nav>
		);
};


Nav.propTypes = {
    openMenu: PropTypes.bool
};

export default Nav;

